package com.company.shape;


public interface Drawable {
    int NUMBER_OF_COLORS=256;
    void draw();
}
