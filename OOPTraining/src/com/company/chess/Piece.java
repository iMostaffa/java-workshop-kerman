package com.company.chess;

/**
 * Created by manp on 8/8/16.
 */
public abstract class Piece {
    protected OnPieceMoveListener onPieceMoveListener=null;
    public void move(int x,int y){
        //moved
        if(onPieceMoveListener!=null){
            onPieceMoveListener.onMove(x,y);
        }

    }

    public void setOnPieceMoveListener(OnPieceMoveListener onPieceMoveListener) {
        this.onPieceMoveListener = onPieceMoveListener;
    }

    public interface OnPieceMoveListener{
        public void onMove(int newX,int newY);
    }
}
