package com.company.datatypes;

import java.math.BigDecimal;

/**
 * Created by manp on 8/4/16.
 */
public class PrimitiveDataType {
    public void numericDataType(){

        //byte
        byte b1=4;
        //binary
        byte b2=0b0010;
        //octal
        byte b3=024;
        //hex
        byte b4=0x1F;

        //short
        short s1=2000;

        //int
        int i1=32000;

        //Long
        //long l1=3000000000; //not ok (type casting)
        long l2=3000000000L; //ok


        //float
        float f1=3.14f;

        //double
        double d1= 345.234d;
    }

    public void numericDTConvertion(){
        // upward type conversion  (upcasting)
        byte b1=12;
        short s1=b1;
        int i1=b1;
        long l1=i1;
        float f1=l1;
        double d1=f1;



        //downward type conversion (downcasting)

        double d2=128.23d;
        //float f2=d2; compiler error
        float f2=(float)d2; //f2=> 128.23


        short s2=128;
        byte b2= (byte) s2;

    }

    public void decimalAnomaly(){
        double d1=156.34;
        BigDecimal bd=new BigDecimal(d1);
        System.out.println(bd.toString());
    }




    public void booleanDT(){
        boolean b1=true;
        boolean b2=false;
        boolean b3=b1&&b2;
        boolean b4=3>5;
        boolean b5; //b5=>false
    }


    public void charDT(){
        char c1='1';
        char c2='A';
        char c3='d';
        char c4;// 0000
        char c5='\u0000';
        char c6='\u9999';
        System.out.println(c6);
    }
}
