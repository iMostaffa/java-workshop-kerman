package com.company.datatypes;

import com.company.shape.Circle;

/**
 * Created by manp on 8/4/16.
 */
public class HelperClasses {
    public void numericHelpers(){
        int i=12;
        Integer i2=12;
        Integer i3=new Integer(12);

        i3.floatValue();
        i3.byteValue();
        i3.toString();

        Double d2=Double.parseDouble("14.54");
    }

    public void ObjectRefrence(){
        Circle c1=new Circle(5);
        System.out.println(c1.radius);
        Circle c2=c1;
        c2.radius=10;
        System.out.println(c1.radius);
        Circle c3=new Circle(10);


        System.out.println(c1==c3);
    }

    public void booleanHelper(){
        Boolean b1=false;
        Boolean b2=new Boolean(true);
        Boolean b3=new Boolean("false");
        Boolean b4=new Boolean("1"); //false
        System.out.println(b4.toString());

        Boolean b5=Boolean.parseBoolean("true");
        System.out.println(b5.toString());
    }

    public void charHelper(){
        Character c1='a';
        Character c2=new Character('4');

        char chars[]={'h','e','l','l','o'};
        String s1=new String(chars);
    }
}
