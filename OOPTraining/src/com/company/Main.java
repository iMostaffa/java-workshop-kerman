package com.company;


import com.company.chess.King;
import com.company.chess.Piece;
import com.company.exceptionhandling.Calculator;
import com.company.shape.Circle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Circle> circles=new ArrayList<>();
        circles.add(new Circle(1));
        circles.add(new Circle(2));
        circles.add(new Circle(3));
        circles.add(new Circle(4));

        someMethod(circles);


        Circle c1=new Circle(1);
        Circle c2=new Circle(2);
        Circle c3=new Circle(3);
        Circle c4=new Circle(4);
        anotherMethod(c1,c2,c3,c4);

    }

    public static void someMethod(ArrayList<Circle> cs){

    }

    public static void anotherMethod(Circle ... cs){

    }




}