package com.company.student;

public enum Grade {
    HIGH (1),
    MED (2),
    LOW (3);

    private int value;

    private Grade(int level){
        value=level;
    }

    public String getValue(){
        return value+"";
    }
}
