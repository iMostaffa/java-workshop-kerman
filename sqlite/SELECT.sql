SELECT *
FROM User
WHERE _id=7;


-- case insensitive
SELECT * FROM User
WHERE  lower(first_name)=lower("eric") AND last_name="Stanley";


-- russian users
-- upper
SELECT first_name,last_name
FROM User
WHERE upper(country)="RUSSIA";

-- more than 4500
SELECT first_name,last_name
FROM User
WHERE balance>=4500;

-- like with %
SELECT *
FROM User
WHERE lower(first_name) LIKE "sh%";


-- like with _
SELECT *
FROM User
WHERE lower(last_name) LIKE "____er";
-- 6 digit ends with er



-- union
SELECT *
FROM User
WHERE lower(last_name) LIKE "_o%"

UNION

SELECT *
FROM User
WHERE lower(last_name) LIKE "%er";





-- not distinct
SELECT first_name from User WHERE first_name="Eric";

-- distinct
SELECT DISTINCT first_name FROM User WHERE first_name="Eric";

-- between
SELECT *
FROM User
WHERE balance>2000 AND balance<20000;
SELECT *
FROM User
WHERE balance BETWEEN 2000 AND 20000;

SELECT *
FROM User
WHERE birthday BETWEEN "1990-1-1" AND "1999-12-29";



-- timestamp
SELECT CURRENT_TIMESTAMP;

-- isnull
SELECT * from "Order"
WHERE description ISNULL;


-- in
SELECT *
FROM User
WHERE _id IN (2,3,5,8) AND User.country="China";

-- not
SELECT *
FROM User
WHERE _id NOT IN (2,3,5,8) ;


-- in with nested query
-- find users with order
SELECT _id,first_name,last_name,email
FROM User
WHERE  _id IN (
  SELECT DISTINCT user
  FROM "Order"
);


-- order of users with balance less that 2500
SELECT * FROM "Order"
WHERE user IN (
  SELECT User._id
  FROM User
  WHERE balance>2500
);


-- users with no order
SELECT email from User
WHERE _id NOT IN (
  SELECT user from "Order"
);


-- always true
SELECT *
FROM User
WHERE 1=1;

-- exist
SELECT * FROM User
WHERE EXISTS(
  SELECT * FROM "Order"
  WHERE "Order".user=User._id
);


-- Aggregate Functions

-- count
SELECT count(*)
FROM User;


SELECT count(*)
FROM User
WHERE balance>2500;

--average
SELECT avg(balance) as average
FROM User;

-- sum
SELECT sum(balance) as sum
FROM User;

-- min
SELECT min(balance) as min
FROM User;

-- max
SELECT max(balance) as max
FROM User;


---------------- ORDER BY -------------
SELECT *
FROM User
ORDER BY first_name , last_name DESC  ;

SELECT *
FROM User
ORDER BY first_name ASC , _id DESC  ;


---------------- LIMIT ----------------
SELECT * FROM User
WHERE 1=1
LIMIT 5 ;
SELECT * FROM User
WHERE 1=1
LIMIT 5 , 5;
SELECT * FROM User
WHERE 1=1
LIMIT 5 OFFSET 5;





----------------- GROUP ---------------
SELECT country,count(*) FROM User
GROUP BY country;

SELECT country,balance,COUNT(*) FROM User
GROUP BY country,balance;

SELECT country,count(User.country) FROM User
GROUP BY country,balance;

SELECT country FROM User
GROUP BY country
HAVING COUNT(*)>1;



------------------- JOIN ----------------
SELECT * from User JOIN "Order";

SELECT * from User JOIN "Order" ON User._id = "Order".user;
SELECT * FROM User JOIN "Order" USING (_id);

SELECT User.first_name,"Order".pay_id from User JOIN "Order" ON User._id = "Order".user;


SELECT "Order"._id FROM User JOIN "Order" ON User._id = "Order".user
WHERE first_name="Gregory";



SELECT first_name,last_name,email,name FROM User JOIN "Order" JOIN Service
    ON User._id="Order".user AND "Order".service=Service._id;