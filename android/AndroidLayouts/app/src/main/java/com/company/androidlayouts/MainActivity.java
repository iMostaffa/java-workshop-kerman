package com.company.androidlayouts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText editText;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText= (EditText) findViewById(R.id.input_et);
        textView= (TextView) findViewById(R.id.message);

    }

    public void onCopyBtnClick(View view) {
        String str=editText.getText().toString();
        textView.setText(str);
    }
}
