package com.company.activitylifecycle;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public String localVariable="first value";

    final String TAG="TEST";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, ">>>>>>>>> "+localVariable);
        Log.d(TAG, "onCreate: ");
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart: ");
    }

    public void onBtnCLick(View view) {
        Log.d(TAG,"------------------------------");
    }

    public void gotoActivity1(View view) {
        startActivity(new Intent(this,SecondActivity.class));
    }

    public void gotoActivity2(View view) {
        startActivity(new Intent(this,ThirdActivity.class));
    }

    public void onChangeClick(View view) {
        localVariable="second value";
    }


    public void onShowClick(View view) {
        Toast.makeText(this,localVariable,Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("LOCAL_VAR",localVariable);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        localVariable=savedInstanceState.getString("LOCAL_VAR");
    }
}
