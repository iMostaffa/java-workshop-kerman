package com.company.blog;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by manp on 8/11/16.
 */
public class BlogBDHelper extends SQLiteOpenHelper {
    Context context;
    protected final static String DATABASE_NAME="blog.sqlite";
    protected final static int DATABASE_VERSION=1;

    public BlogBDHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(context.getResources().getString(R.string.create_table_post));
        db.execSQL(context.getResources().getString(R.string.create_table_user));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



}
