package com.company.blog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class MainActivity extends Activity{
    ListView listView;
    public ArrayList<Post> posts=new ArrayList<>();
    PostArrayAdapter postArrayAdapter;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





//        sharedPreferences = getSharedPreferences("SETTINGS",MODE_PRIVATE);
//
//        if(!sharedPreferences.getBoolean("DISPLAYED_SPLASH",false)){
//            startActivity(new Intent(this,SplashActivity.class));
//        }
        startActivity(new Intent(this,SplashActivity.class));


        fetchPosts();













        listView= (ListView) findViewById(R.id.listView);

        postArrayAdapter = new PostArrayAdapter(this,posts);
        listView.setAdapter(postArrayAdapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Post p= (Post) parent.getItemAtPosition(position);
                Intent i=new Intent(MainActivity.this,PreviewPostActivity.class);
                i.putExtra("POST",p);
                startActivity(i);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                final Post p= (Post) parent.getItemAtPosition(position);

                AlertDialog.Builder ab=new AlertDialog.Builder(MainActivity.this);

                ab.setTitle("Choose an Action");

//                ab.setMessage("This is a Message");
//
//
//                ab.setPositiveButton("Save", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//
//                ab.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });

                String[] items={"Edit", "Delete"};
                ab.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:

                                Intent i=new Intent(MainActivity.this,EditPostActivity.class);
                                i.putExtra("POST",p);
                                startActivityForResult(i,2);
                                break;

                            case 1:
                                posts.remove(position);
                                postArrayAdapter.notifyDataSetChanged();
                                break;
                        }
                    }
                });
                ab.show();




                return true;
            }
        });
    }

    private void fetchPosts() {
        BlogBDHelper blogDBHelper=new BlogBDHelper(this);
        SQLiteDatabase db=blogDBHelper.getReadableDatabase();

        Cursor cursor=db.query("Post", null, null,null,null,null,"_id DESC");

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            String title=cursor.getString(1);
            String body=cursor.getString(2);
            Boolean faved=cursor.getInt(5)==0?false:true;
            posts.add(new Post(title,body,faved));
            cursor.moveToNext();
        }
    }


    public void onNewPostBtnClick(View view) {
        Intent intent=new Intent(this,NewPostActivity.class);
        intent.setAction("NEW_POST");
        startActivityForResult(intent,1);
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1 && resultCode==RESULT_OK){
            posts.add((Post) data.getSerializableExtra("POST"));
            postArrayAdapter.notifyDataSetChanged();
        }


    }


    public void removePost(int id){
        BlogBDHelper blogDBHelper=new BlogBDHelper(this);
        SQLiteDatabase db=blogDBHelper.getWritableDatabase();
        String[] args={id+""};
        db.delete("Post","where _id=?",args);
    }
    public void updatePost(Post post){
        BlogBDHelper blogDBHelper=new BlogBDHelper(this);
        SQLiteDatabase db=blogDBHelper.getWritableDatabase();

        //db.update()

 //       db.rawQuery("",)
    }
}
