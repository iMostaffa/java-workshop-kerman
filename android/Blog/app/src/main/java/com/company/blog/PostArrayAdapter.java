package com.company.blog;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manp on 8/10/16.
 */
public class PostArrayAdapter extends ArrayAdapter<Post> {
    public PostArrayAdapter(Context context,  List<Post> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Post post=getItem(position);

        if(convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.post_list_item,parent,false);
        }

        ((TextView)convertView.findViewById(R.id.post_item_title)).setText(post.getTitle());
        ((TextView)convertView.findViewById(R.id.post_item_body)).setText(post.getBody());
        ((ImageView)convertView.findViewById(R.id.post_item_fav)).setImageResource(post.getFavorite()?android.R.drawable.star_big_on:android.R.drawable.star_big_off);

        ((ImageView)convertView.findViewById(R.id.post_item_fav)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getItem(position).toggleFav();
                notifyDataSetChanged();
            }
        });




        return  convertView;
    }
}
