package com.company.blog;


import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewPostActivity extends AppCompatActivity {
    EditText newPostTitleET;
    EditText newPostBodyET;
    Button newPostSaveBT;
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        newPostTitleET=(EditText) findViewById(R.id.new_post_title);
        newPostBodyET=(EditText) findViewById(R.id.new_post_body);
        newPostSaveBT=(Button) findViewById(R.id.new_post_save_btn);

        actionBar=getSupportActionBar();
        actionBar.setTitle("New Post");

        setChangeListeners();



//        Intent i=getIntent();
//        int j=i.getIntExtra("num_posts",0);
//        Toast.makeText(NewPostActivity.this, j+"", Toast.LENGTH_SHORT).show();
//        i.getAction();

    }


    protected void setChangeListeners(){
        newPostTitleET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==0 || newPostBodyET.getText().length()==0){
                    newPostSaveBT.setEnabled(false);
                }
                else{
                    newPostSaveBT.setEnabled(true);
                }

                actionBar.setTitle(s.length()>0?s.toString():"New Post");

                actionBar.setBackgroundDrawable(
                        new ColorDrawable(getResources().getColor(
                                s.length()==0?R.color.inactive_action_bar:R.color.active_action_bar
                        ))
                );


            }
        });

        newPostBodyET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                newPostSaveBT.setEnabled(s.length()>0 && newPostTitleET.getText().length()>0);
                newPostBodyET.setMinLines(newPostBodyET.length()>0?7:3);



            }
        });
    }

    public void onSavePostBtnClick(View view) {
        // save prog
        Post p=new Post(newPostTitleET.getText().toString(),newPostBodyET.getText().toString(),false);

        Intent intent=new Intent();
//        intent.putExtra("POST_TITLE",newPostTitleET.getText().toString());
//        intent.putExtra("POST_BODY",newPostBodyET.getText().toString());
        intent.putExtra("POST",p);

        setResult(RESULT_OK,intent);


        saveToDb(p);

        finish();

    }

    public void saveToDb(Post post){
        BlogBDHelper blogBDHelper=new BlogBDHelper(this);
        SQLiteDatabase db=blogBDHelper.getWritableDatabase();

        ContentValues fields=new ContentValues();
        fields.put("title",post.title);
        fields.put("body",post.body);
        fields.put("author","adasdad");
        fields.put("faved",0);




        long newId=db.insert("Post","null",fields);


        db.close();


    }

}
