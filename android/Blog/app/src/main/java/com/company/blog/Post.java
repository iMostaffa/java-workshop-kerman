package com.company.blog;

import java.io.Serializable;

/**
 * Created by manp on 8/10/16.
 */
public class Post implements Serializable {
    Integer id=0;
    String title="";
    String body="";
    Boolean favorite =false;

    public Post(String title, String body, Boolean favorite) {
        this.title = title;
        this.body = body;
        this.favorite = favorite;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public void toggleFav(){
        favorite=!favorite;
    }
}
