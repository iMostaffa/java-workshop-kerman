package com.company.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ((TextView) findViewById(R.id.message)).setText("salam!");
    }

    public void onBtnClick(View view) {
        TextView message;
        message = (TextView) findViewById(R.id.message);
        message.setText("سلام دوستان");
    }
}
